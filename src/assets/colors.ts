const colors = {
  WHITE: '#FFFFFF',
  BLACK: '#555555',
  GRAY: '#707070',
  GRAY2: '#D9D9D9',
  GRAY3: '#BFBFBF',
  RED: '#FF0000',
  BLUE: '#1D58DE',
};

export default colors;
