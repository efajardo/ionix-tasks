import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useAppSelector} from '../store/hooks';
import colors from '../assets/colors';
import {useDispatch} from 'react-redux';
import {logout} from '../store/slices/user';

const Account = () => {
  const user = useAppSelector(state => state.user);
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Name</Text>
      <Text style={styles.text}>{user.name}</Text>
      <Text style={styles.label}>username</Text>
      <Text style={styles.text}>{user.username}</Text>
      <Text style={styles.label}>is admin?</Text>
      <Text style={styles.text}>{user.isAdmin ? 'YES' : 'NO'}</Text>

      <View style={styles.logoutContainer}>
        <TouchableOpacity onPress={() => handleLogout()}>
          <Text style={styles.logoutText}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    padding: 16,
  },
  label: {
    color: colors.GRAY,
    fontSize: 16,
    fontWeight: 'bold',
  },
  text: {
    color: colors.GRAY3,
    fontSize: 16,
    marginBottom: 20,
  },
  logoutText: {
    color: colors.BLUE,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  logoutContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    marginBottom: 70,
  },
});

export default Account;
