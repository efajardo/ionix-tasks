import React from 'react';
import {FlatList, View, StyleSheet} from 'react-native';
import colors from '../assets/colors';
import AddTask from '../features/tasks/AddTask';
import DeleteTask from '../features/tasks/DeleteTask';
import UpdateTask from '../features/tasks/UpdateTask';
import ViewTask from '../features/tasks/ViewTask';
import useGetTasks from '../queries/tasks/useGetTasks';
import {useAppSelector} from '../store/hooks';
import {canDeleteOrUpdate} from '../utils/canDeleteOrUpdate';
import {canChangeStatus} from '../utils/canChangeStatus';
import ChangeStatusTask from '../features/tasks/ChangeStatusTask';
import Loading from '../components/Loading';
import TaskItem from '../components/TaskItem';

interface TaskTypeProps {
  id: number;
  title: string;
  description: string;
  deadline: string;
  status: string;
}

const Tasks = () => {
  const user = useAppSelector(state => state.user);
  const filter = {
    isAdmin: user.isAdmin,
    userId: user.id,
  };
  const {data: tasks, isLoading} = useGetTasks(filter);
  // console.log('tasks', tasks);
  const _renderItem = ({item}: {item: TaskTypeProps}) => {
    return (
      <View style={styles.itemContainer}>
        <TaskItem task={item} />
        <View style={styles.actions}>
          <ViewTask taskId={item.id} />
          {canDeleteOrUpdate(user.isAdmin, item.status) && (
            <>
              <UpdateTask task={item} />
              <DeleteTask taskId={item.id} />
            </>
          )}

          {canChangeStatus(user.isAdmin, item.status) && (
            <ChangeStatusTask task={item} />
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Loading visible={isLoading} />
      <FlatList
        data={tasks}
        style={styles.listStyle}
        renderItem={_renderItem}
        keyExtractor={item => item.id.toString()}
      />
      {user.isAdmin && (
        <View style={styles.btnContainer}>
          <AddTask />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },
  listStyle: {padding: 16},
  itemContainer: {
    marginVertical: 5,
    marginHorizontal: 5,
    marginBottom: 16,
    paddingVertical: 5,
    paddingHorizontal: 16,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 35,
    shadowColor: colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 10,
  },
  actions: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    borderTopWidth: 1,
    borderTopColor: colors.GRAY2,
    padding: 16,
    marginTop: 16,
  },
  btnContainer: {
    marginHorizontal: 16,
    marginVertical: 10,
  },
});

export default Tasks;
