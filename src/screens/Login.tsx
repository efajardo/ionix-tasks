import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Button from '../components/Button';
import {useForm, SubmitHandler} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import colors from '../assets/colors';
import Form from '../components/Form';
import useLogin from '../queries/auth/useLogin';
import {login} from '../store/slices/user';
import * as yup from 'yup';

import Icon from 'react-native-vector-icons/Ionicons';

import {useAppDispatch} from '../store/hooks';
import Loading from '../components/Loading';

const schema = yup.object({
  username: yup.string().required(),
  password: yup.string().required(),
});

export type LoginProps = {
  username: string;
  password: string;
};

const Login = () => {
  const [error, setError] = useState('');
  const dispatch = useAppDispatch();

  const {
    control,
    register,
    handleSubmit,
    formState: {errors},
  } = useForm<LoginProps>({
    resolver: yupResolver(schema),
  });

  const {isLoading, tryLogin} = useLogin({
    onSuccess: user => {
      const data = {...user, isAuthenticated: true};
      dispatch(login(data));
    },
    onError: e => setError(e),
  });

  const inputs: any = [
    {
      name: 'username',
      placeholder: 'Username',
      type: 'text',
    },
    {
      name: 'password',
      placeholder: 'Password',
      type: 'password',
    },
  ];

  const onLogin: SubmitHandler<LoginProps> = data => {
    setError('');
    tryLogin(data);
  };

  return (
    <View style={styles.container}>
      <Loading visible={isLoading} />
      <Text style={styles.title}>
        Login <Icon name={'rocket'} size={25} />
      </Text>
      <Form
        inputs={inputs}
        errors={errors}
        register={register}
        control={control}
      />

      <Button title={'Login'} onPress={handleSubmit(onLogin)} />
      {error && <Text style={styles.error}>{error}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: colors.GRAY,
    margin: 10,
    fontSize: 30,
    marginBottom: 40,
  },
  error: {
    color: colors.RED,
  },
});

export default Login;
