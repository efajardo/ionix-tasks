import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import colors from '../assets/colors';
import {dateFormat} from '../utils/dateFormat';

interface TaskItemTypesProps {
  task: {
    title: string;
    description: string;
    deadline: string;
    status: string;
  };
}

const TaskItem = ({task}: TaskItemTypesProps) => {
  return (
    <View>
      <Text style={styles.label}>Title</Text>
      <Text style={styles.text}>{task.title}</Text>

      <Text style={styles.label}>Description</Text>
      <Text style={styles.text}>{task.description}</Text>
      <View style={styles.section}>
        <View>
          <Text style={styles.label}>Deadline</Text>
          <Text style={styles.text}>{dateFormat(task.deadline)}</Text>
        </View>
        <View>
          <Text style={styles.label}>Status</Text>
          <View style={styles.globeContainer}>
            <Text style={styles.globeText}>{task.status}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  section: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 16,
    color: colors.GRAY,
    fontWeight: 'bold',
  },
  text: {
    color: colors.GRAY3,
    fontSize: 16,
    marginBottom: 16,
  },
  globeContainer: {
    backgroundColor: colors.GRAY3,
    borderRadius: 15,
  },
  globeText: {
    color: colors.WHITE,
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    paddingVertical: 2,
    paddingHorizontal: 16,
  },
});

export default TaskItem;
