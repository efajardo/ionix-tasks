import React, {useState} from 'react';
import {Controller, UseFormRegister} from 'react-hook-form';
import {
  FlatList,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../assets/colors';

interface ItemTypes {
  label: string;
  value: string | number;
}

interface SelectorPropsTypes {
  control: any;
  name: string;
  value?: string | number;
  placeholder: string;
  options?: Array<ItemTypes>;
  editable?: boolean;
  register?: UseFormRegister<any>;
}

const Selector = ({
  control,
  name,
  value,
  options,
  editable,
  placeholder,
}: SelectorPropsTypes) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [selected, setSelected] = useState<string | number>(value ? value : '');
  const _renderItem = ({item}: {item: ItemTypes}) => {
    return (
      <Controller
        control={control}
        render={({field: {onChange}}) => (
          <TouchableOpacity
            style={styles.itemContainer}
            onPress={() => {
              setModalVisible(false);
              onChange(item.value);
              setSelected(item.value);
            }}>
            <Text style={styles.label}>{item.label}</Text>
          </TouchableOpacity>
        )}
        name={name}
      />
    );
  };

  return (
    <>
      <Modal animationType="none" visible={modalVisible}>
        <View style={styles.centeredView}>
          <View onStartShouldSetResponder={(): boolean => true}>
            <Text style={styles.mainTitle}>{placeholder}</Text>
            <FlatList
              data={options}
              renderItem={_renderItem}
              keyExtractor={item => item.value.toString()}
            />
          </View>
        </View>
      </Modal>
      <TouchableOpacity
        style={styles.container}
        onPress={() => editable && setModalVisible(true)}>
        <Text style={styles.placeholder}>{placeholder}</Text>
        <Text style={[styles.selectedText, !editable && styles.disabled]}>
          {selected
            ? options?.find(opt => opt.value === selected)?.label
            : 'Select'}
        </Text>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    marginTop: 30,
    flex: 1,
  },
  itemContainer: {
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: colors.GRAY,
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: colors.GRAY2,
    borderWidth: 1,
    borderRadius: 8,
    padding: 16,
    marginBottom: 15,
  },
  mainTitle: {
    fontSize: 25,
    textAlign: 'center',
  },
  placeholder: {
    color: colors.GRAY,
  },
  selectedText: {
    color: colors.BLUE,
  },
  disabled: {
    backgroundColor: colors.GRAY,
  },
});

export default Selector;
