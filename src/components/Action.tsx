import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

interface PropsActionTypes {
  text: string;
  onPress: () => void;
}

const Action = ({text, onPress}: PropsActionTypes) => {
  return (
    <TouchableOpacity onPress={() => onPress()}>
      <Text style={styles.textStyle}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    color: 'blue',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Action;
