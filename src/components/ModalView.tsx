import React from 'react';
import {Modal, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import colors from '../assets/colors';
import styles from '../assets/styles';

interface ModalViewPropTypes {
  child: JSX.Element;
  onClose: () => void;
  visibleModal: boolean;
}

const ModalView = ({child, onClose, visibleModal}: ModalViewPropTypes) => {
  return (
    <Modal animationType="fade" transparent={true} visible={visibleModal}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          {child}
          <TouchableOpacity onPress={() => onClose()}>
            <Text style={stylesModal.closeText}>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const stylesModal = StyleSheet.create({
  closeText: {
    color: colors.BLUE,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default ModalView;
