import React from 'react';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';
import colors from '../assets/colors';

interface PropButtonTypes {
  title: string;
  onPress: () => void;
}

const Button = (props: PropButtonTypes) => {
  const {title, onPress} = props;
  return (
    <View style={styles.btnContainer}>
      <TouchableOpacity style={styles.buttonStyle} onPress={onPress}>
        <Text style={styles.buttonTextStyle}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  btnContainer: {
    flexDirection: 'row',
  },
  buttonStyle: {
    backgroundColor: colors.BLUE,
    padding: 15,
    width: '100%',
    marginBottom: 10,
    borderRadius: 24,
    alignSelf: 'center',
  },
  buttonTextStyle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Button;
