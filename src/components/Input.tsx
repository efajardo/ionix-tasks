import React from 'react';
import {Controller, UseFormRegister} from 'react-hook-form';
import {TextInput, StyleSheet} from 'react-native';

import colors from '../assets/colors';

interface PropInputTypes {
  control: any;
  name: string;
  placeholder: string;
  type: string;
  editable?: boolean;
  register?: UseFormRegister<any>;
}

const Input = ({
  control,
  name,
  placeholder,
  type,
  editable,
}: PropInputTypes) => {
  return (
    <Controller
      control={control}
      render={({field: {onChange, value}}) => (
        <TextInput
          editable={editable}
          autoCapitalize="none"
          style={styles.inputStyle}
          placeholder={placeholder}
          value={value}
          onChangeText={onChange}
          secureTextEntry={type === 'password'}
        />
      )}
      name={name}
    />
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    borderColor: colors.GRAY2,
    borderWidth: 1,
    padding: 16,
    fontSize: 14,
    marginBottom: 10,
    borderRadius: 8,
  },
});

export default Input;
