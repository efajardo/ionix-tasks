import React from 'react';
import Input from './Input';
import Selector from './Selector';
import {UseFormRegister} from 'react-hook-form';
import {StyleSheet, Text, View} from 'react-native';
import colors from '../assets/colors';

export interface FormTypesProps {
  inputs:
    | Array<{
        name: string;
        placeholder: string;
        value?: string;
        type: string;
        editable?: boolean;
        options?: Array<{value: string; label: string}>;
      }>
    | [];
  errors: {[x: string]: any};
  register: UseFormRegister<any>;
  control: any;
}

const Form = ({inputs, errors, register, control}: FormTypesProps) => {
  let elems: Array<JSX.Element> = [];

  // console.log('Errors>>', errors, inputs);

  inputs.forEach((input, index) => {
    switch (input.type) {
      case 'text':
      case 'password':
        elems.push(
          <View key={index}>
            <Input {...input} register={register} control={control} />
            <Text style={styles.errorLabel}>{errors[input.name]?.message}</Text>
          </View>,
        );
        break;
      case 'select':
        elems.push(
          <View key={index}>
            <Selector {...input} register={register} control={control} />
            <Text style={styles.errorLabel}>{errors[input.name]?.message}</Text>
          </View>,
        );
        break;
    }
  });
  return <>{elems}</>;
};

const styles = StyleSheet.create({
  errorLabel: {
    color: colors.RED,
    marginBottom: 5,
  },
});

export default Form;
