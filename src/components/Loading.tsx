import React from 'react';
import {ActivityIndicator, Modal, StyleSheet, View} from 'react-native';
import colors from '../assets/colors';

interface PropsTypesLoading {
  visible: boolean;
}

const Loading = ({visible}: PropsTypesLoading) => {
  return (
    <Modal transparent={true} visible={visible} animationType={'fade'}>
      <View style={styles.centeredView}>
        <ActivityIndicator size={'large'} color={colors.BLUE} />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
});

export default Loading;
