import {useQuery} from 'react-query';
import axiosInstance from '../axiosInstance';

const key = 'users';

const useGetUsers = () => {
  return useQuery([key], async () => {
    const {data} = await axiosInstance.get('/users?isAdmin=false');
    return data;
  });
};

export default useGetUsers;
