import {useQuery} from 'react-query';
import axiosInstance from '../axiosInstance';

const key = 'statuses';

const useGetStatuses = () => {
  return useQuery([key], async () => {
    const {data} = await axiosInstance.get('/statuses');
    return data;
  });
};

export default useGetStatuses;
