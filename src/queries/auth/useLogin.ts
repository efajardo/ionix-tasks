import {useMutation} from 'react-query';
import axiosInstance from '../axiosInstance';

interface LoginPropsTypes {
  username: string;
  password: string;
}

interface UserPropsTypes {
  id: number;
  name: string;
  username: string;
  password: string;
  isAdmin: boolean;
  isAuthenticated: boolean;
}

interface useLoginProps {
  onSuccess?: (user: UserPropsTypes) => void;
  onError?: (error: string) => void;
}

const useLogin = ({onSuccess, onError}: useLoginProps) => {
  const mutation = useMutation(
    async (user: LoginPropsTypes) => {
      const {data} = await axiosInstance.get(
        `/users?username=${user.username}&password=${user.password}&is_admin=${
          user.username === 'admin'
        }`,
      );
      return data.pop();
    },
    {
      onSuccess: (user: UserPropsTypes) => {
        if (user === undefined) {
          onError && onError('User does not exist');
        } else {
          onSuccess && onSuccess(user);
        }
      },
      onError: () => {
        onError && onError('please try again');
      },
    },
  );

  const tryLogin = (user: LoginPropsTypes) => {
    mutation.mutate(user);
  };

  return {...mutation, tryLogin};
};

export default useLogin;
