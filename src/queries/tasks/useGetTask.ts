import {useQuery} from 'react-query';
import axiosInstance from '../axiosInstance';

const key = 'task';

const useGetTask = (taskId: number) => {
  return useQuery(
    [key, taskId],
    async () => {
      const {data} = await axiosInstance.get(`/tasks/${taskId}`);
      return data;
    },
    {
      enabled: false,
    },
  );
};

export default useGetTask;
