import {useMutation, useQueryClient} from 'react-query';
import axiosInstance from '../axiosInstance';

export type useAddParamProps = {
  onSuccess?: () => void;
  onError?: () => void;
};

export const useDeleteTask = ({onError, onSuccess}: useAddParamProps) => {
  const queryClient = useQueryClient();

  const mutation = useMutation(
    async (idTask: number) => {
      const {data} = await axiosInstance.delete(`/tasks/${idTask}`);
      return data;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(['tasks']);
        onSuccess && onSuccess();
      },
      onError: () => {},
    },
  );

  const tryDeleteTask = (idTask: number) => {
    mutation.mutate(idTask);
  };

  return {tryDeleteTask, ...mutation};
};
