import {useQuery} from 'react-query';
import axiosInstance from '../axiosInstance';

const key = 'tasks';

interface filterTypes {
  userId: number;
  isAdmin: boolean;
}

const useGetTasks = ({userId, isAdmin}: filterTypes) => {
  return useQuery([key, userId], async () => {
    const url = isAdmin ? '/tasks' : `/tasks?user_id=${userId}`;
    const {data} = await axiosInstance.get(url);
    return data;
  });
};

export default useGetTasks;
