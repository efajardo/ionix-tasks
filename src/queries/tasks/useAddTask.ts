import {useQueryClient, useMutation} from 'react-query';
import axiosInstance from '../axiosInstance';
import {TaskProps} from '../../features/tasks/AddTask';

const key = 'tasks';

interface useAddTaskProps {
  onSuccess?: () => void;
  onError?: () => void;
}

const useAddTask = ({onSuccess, onError}: useAddTaskProps) => {
  const queryClient = useQueryClient();

  const mutation = useMutation(
    async (task: TaskProps) => {
      const {data} = await axiosInstance.post('/tasks', task);
      return data;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries([key]);
        onSuccess && onSuccess();
      },
      onError: () => {
        onError && onError();
      },
    },
  );

  const tryAddTask = (task: TaskProps) => {
    mutation.mutate(task);
  };

  return {...mutation, tryAddTask};
};

export default useAddTask;
