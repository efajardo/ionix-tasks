import {useQueryClient, useMutation} from 'react-query';
import axiosInstance from '../axiosInstance';
import {TaskProps} from '../../features/tasks/AddTask';

const key = 'tasks';

interface useUpdateTaskProps {
  onSuccess?: () => void;
  onError?: () => void;
}

const useUpdateTask = ({onSuccess, onError}: useUpdateTaskProps) => {
  const queryClient = useQueryClient();

  const mutation = useMutation(
    async (task: TaskProps) => {
      const {data} = await axiosInstance.patch(`/tasks/${task.id}`, task);
      return data;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries([key]);
        onSuccess && onSuccess();
      },
      onError: () => {
        onError && onError();
      },
    },
  );

  const tryUpdateTask = (task: TaskProps) => {
    mutation.mutate(task);
  };

  return {...mutation, tryUpdateTask};
};

export default useUpdateTask;
