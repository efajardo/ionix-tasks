import axios from 'axios';
import Config from 'react-native-config';
console.log('URL', Config.API_URL);
const axiosInstance = axios.create({
  baseURL: Config.API_URL,
});

export default axiosInstance;
