import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useAppSelector} from '../store/hooks';

import Login from '../screens/Login';
import Recovery from '../screens/Recovery';
import Tasks from '../screens/Tasks';
import Account from '../screens/Account';

const Stack = createNativeStackNavigator();

const Navigator = () => {
  const user = useAppSelector(state => state.user);
  console.log('User', user);
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={user.isAuthenticated ? 'Tasks' : 'Login'}>
        {!user.isAuthenticated && (
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
        )}
        {user.isAuthenticated && (
          <>
            <Stack.Screen
              name="Tasks"
              component={Tasks}
              options={({navigation}) => ({
                headerLeft: () => <></>,
                headerRight: () => {
                  return (
                    <TouchableOpacity
                      onPress={() => navigation.navigate('Account')}>
                      <Icon name={'cog'} size={25} />
                    </TouchableOpacity>
                  );
                },
              })}
            />
            <Stack.Screen name="Account" component={Account} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
