import moment from 'moment';

export const dateFormat = (date: string, format?: string): string => {
  return moment(date).format(format ? format : 'DD/MM/YYYY');
};
