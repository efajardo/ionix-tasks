import moment from 'moment';

export const isEditable = (date: string) => {
  return moment(date).isAfter();
};
