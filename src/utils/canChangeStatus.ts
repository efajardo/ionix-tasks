export const canChangeStatus = (isAdmin: boolean, status: string) => {
  return !isAdmin && status !== 'assigned';
};
