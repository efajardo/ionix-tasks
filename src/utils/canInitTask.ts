export const canInitTask = (isAdmin: boolean, status: string) => {
  return !isAdmin && status === 'assigned';
};
