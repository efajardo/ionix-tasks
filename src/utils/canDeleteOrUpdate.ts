export const canDeleteOrUpdate = (isAdmin: boolean, status: string) => {
  return isAdmin && (status === 'new' || status === 'assigned');
};
