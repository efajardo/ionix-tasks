import React from 'react';
import {Alert} from 'react-native';
import Action from '../../components/Action';
import {useDeleteTask} from '../../queries/tasks/useDeleteTask';

interface propsTypes {
  taskId: number;
}

const DeleteTask = ({taskId}: propsTypes) => {
  const {tryDeleteTask} = useDeleteTask({
    onError: () => {},
    onSuccess: () => {
      Alert.alert('Task', 'Task has been remove successfully');
    },
  });

  const handleRemove = () => {
    tryDeleteTask(taskId);
  };

  return <Action text={'Remove'} onPress={() => handleRemove()} />;
};

export default DeleteTask;
