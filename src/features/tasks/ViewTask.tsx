import React, {useState} from 'react';
import {Alert, ActivityIndicator} from 'react-native';
import Action from '../../components/Action';
import Button from '../../components/Button';
import useGetTask from '../../queries/tasks/useGetTask';
import useUpdateTask from '../../queries/tasks/useUpdateTask';
import {useAppSelector} from '../../store/hooks';
import {canInitTask} from '../../utils/canInitTask';
import {TaskProps} from './AddTask';

import ModalView from '../../components/ModalView';
import colors from '../../assets/colors';
import TaskItem from '../../components/TaskItem';

interface propsTypes {
  taskId: number;
}

const ViewTask = ({taskId}: propsTypes) => {
  const user = useAppSelector(state => state.user);

  const [visibleModal, setVisibleModal] = useState(false);
  const {data, isFetching, refetch} = useGetTask(taskId);

  const {tryUpdateTask, isLoading} = useUpdateTask({
    onSuccess: () => {
      Alert.alert('Task', 'Task has been initialized');
      setVisibleModal(false);
    },
    onError: () => {},
  });

  const handleInitTask = (task: TaskProps) => {
    tryUpdateTask({...task, status: 'init'});
  };

  return (
    <>
      <ModalView
        child={
          <>
            {(isFetching || isLoading) && <ActivityIndicator />}
            {!isFetching && (
              <>
                <TaskItem task={data} />
                {canInitTask(user.isAdmin, data?.status) && (
                  <Button
                    title={'Initialize'}
                    onPress={() => handleInitTask(data)}
                  />
                )}
              </>
            )}
          </>
        }
        visibleModal={visibleModal}
        onClose={() => setVisibleModal(false)}
      />
      <Action
        text={'View'}
        onPress={() => {
          refetch();
          setVisibleModal(true);
        }}
      />
    </>
  );
};

export default ViewTask;
