import React, {useState} from 'react';
import {ActivityIndicator, Alert} from 'react-native';
import Form from '../../components/Form';
import useGetUsers from '../../queries/users/useGetUsers';
import useUpdateTask from '../../queries/tasks/useUpdateTask';
import Button from '../../components/Button';
import {TaskProps} from './AddTask';
import Action from '../../components/Action';

import ModalView from '../../components/ModalView';
import {SubmitHandler, useForm} from 'react-hook-form';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';

const schema = yup.object({
  title: yup.string().required(),
  description: yup.string().required(),
  deadline: yup.string().required(),
});

const UpdateTask = ({task}: {task: TaskProps}) => {
  const {tryUpdateTask, isLoading} = useUpdateTask({
    onSuccess: () => {
      Alert.alert('Task', 'Task has been updated successfuly');
      setVisibleModal(false);
    },
    onError: () => {
      Alert.alert('Task', 'An error has ocurred. Please try again.');
    },
  });

  const {
    control,
    register,
    handleSubmit,
    formState: {errors},
  } = useForm<TaskProps>({
    resolver: yupResolver(schema),
    values: {
      ...task,
    },
  });

  const [visibleModal, setVisibleModal] = useState(false);

  const {data: users} = useGetUsers();

  const inputs = [
    {
      name: 'title',
      placeholder: 'Title',
      type: 'text',
    },
    {
      name: 'description',
      placeholder: 'Description',
      type: 'text',
    },
    {
      name: 'deadline',
      placeholder: 'Deadline',
      type: 'text',
    },
    {
      name: 'user_id',
      placeholder: 'Select operator',
      type: 'select',
      value: task.user_id,
      options:
        users &&
        users.map(user => {
          return {value: user.id, label: user.name};
        }),
      editable: true,
    },
  ];

  const handleUpdate: SubmitHandler<TaskProps> = data => {
    const dataAux = {
      ...data,
      status: data.user_id ? 'assigned' : 'new',
    };
    console.log(dataAux);
    tryUpdateTask(dataAux);
  };

  return (
    <>
      <ModalView
        child={
          <>
            <Form
              inputs={inputs}
              errors={errors}
              register={register}
              control={control}
            />
            {isLoading && <ActivityIndicator />}
            <Button title={'Submit'} onPress={handleSubmit(handleUpdate)} />
          </>
        }
        visibleModal={visibleModal}
        onClose={() => setVisibleModal(false)}
      />
      <Action text={'Edit'} onPress={() => setVisibleModal(true)} />
    </>
  );
};

export default UpdateTask;
