import React, {useState} from 'react';
import {ActivityIndicator, Alert} from 'react-native';
import Form from '../../components/Form';
import useGetStatuses from '../../queries/statuses/useGetStatuses';
import useUpdateTask from '../../queries/tasks/useUpdateTask';
import {TaskProps} from './AddTask';
import {isEditable} from '../../utils/isEditable';
import Action from '../../components/Action';
import ModalView from '../../components/ModalView';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {SubmitHandler, useForm} from 'react-hook-form';
import Button from '../../components/Button';

const schema = yup.object({});

const ChangeStatusTask = ({task}: {task: TaskProps}) => {
  const [visibleModal, setVisibleModal] = useState(false);

  const {data: statuses} = useGetStatuses();
  const {tryUpdateTask, isLoading} = useUpdateTask({
    onSuccess: () => {
      Alert.alert('Task', 'Task has been updated successfully');
      setVisibleModal(false);
    },
    onError: () => {},
  });

  const {
    control,
    register,
    handleSubmit,
    formState: {errors},
  } = useForm<TaskProps>({
    resolver: yupResolver(schema),
    values: {
      ...task,
    },
  });

  const inputs = [
    {
      name: 'status',
      placeholder: 'Select status',
      type: 'select',
      value: task.status,
      options: statuses,
      editable: isEditable(task.deadline),
    },
    {
      name: 'comment',
      placeholder: 'Comment',
      type: 'text',
    },
  ];

  const handleUpdate: SubmitHandler<TaskProps> = data => {
    tryUpdateTask(data);
  };

  return (
    <>
      <ModalView
        child={
          <>
            <Form
              inputs={inputs}
              errors={errors}
              register={register}
              control={control}
            />
            {isLoading && <ActivityIndicator />}
            <Button title={'Submit'} onPress={handleSubmit(handleUpdate)} />
          </>
        }
        visibleModal={visibleModal}
        onClose={() => setVisibleModal(false)}
      />
      <Action text={'Edit'} onPress={() => setVisibleModal(true)} />
    </>
  );
};

export default ChangeStatusTask;
