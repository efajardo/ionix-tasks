import React, {useState} from 'react';
import {ActivityIndicator, Alert} from 'react-native';
import Button from '../../components/Button';
import Form from '../../components/Form';
import useAddTask from '../../queries/tasks/useAddTask';
import useGetUsers from '../../queries/users/useGetUsers';
import ModalView from '../../components/ModalView';

import * as yup from 'yup';
import {SubmitHandler, useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';

export interface TaskProps {
  id?: number;
  title: string;
  description: string;
  deadline: string;
  status?: string;
  comment?: string;
  user_id: number | null;
}

const schema = yup.object({
  title: yup.string().required(),
  description: yup.string().required(),
  deadline: yup.string().required(),
});

const AddTask = () => {
  const {data: users} = useGetUsers();

  const close = () => {
    reset();
    setVisibleModal(false);
  };

  const {tryAddTask, isLoading} = useAddTask({
    onSuccess: () => {
      Alert.alert('Task', 'Task was created successfully');
      close();
    },
    onError: () => {},
  });

  const {
    control,
    register,
    reset,
    handleSubmit,
    formState: {errors},
  } = useForm<TaskProps>({
    resolver: yupResolver(schema),
  });

  const [visibleModal, setVisibleModal] = useState(false);

  const handleSubmitTask: SubmitHandler<TaskProps> = data => {
    console.log({
      ...data,
      status: data.user_id ? 'assigned' : 'new',
    });
    tryAddTask({
      ...data,
      status: data.user_id ? 'assigned' : 'new',
    });
  };

  const inputs: any = [
    {
      name: 'title',
      placeholder: 'Title',
      type: 'text',
    },
    {
      name: 'description',
      placeholder: 'Description',
      type: 'text',
    },
    {
      name: 'deadline',
      placeholder: 'Deadline',
      type: 'text',
    },
    {
      name: 'user_id',
      placeholder: 'Select user',
      type: 'select',
      editable: true,
      options: users?.map((user: any) => {
        return {
          value: user.id,
          label: user.name,
        };
      }),
    },
  ];

  return (
    <>
      <ModalView
        child={
          <>
            <Form
              inputs={inputs}
              errors={errors}
              register={register}
              control={control}
            />
            {isLoading && <ActivityIndicator />}
            <Button
              title={'Create Task'}
              onPress={handleSubmit(handleSubmitTask)}
            />
          </>
        }
        visibleModal={visibleModal}
        onClose={() => close()}
      />
      <Button title={'Add Task'} onPress={() => setVisibleModal(true)} />
    </>
  );
};

export default AddTask;
