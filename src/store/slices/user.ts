import {createSlice} from '@reduxjs/toolkit';
import type {PayloadAction} from '@reduxjs/toolkit';

export type UserState = {
  id: number;
  name: string;
  username: string;
  password: string;
  isAuthenticated: boolean;
  isAdmin: boolean;
  token?: string;
};

const initialState: UserState = {
  id: 0,
  name: '',
  username: '',
  password: '',
  isAuthenticated: false,
  isAdmin: false,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    login: (state, action: PayloadAction<UserState>) => {
      return {...state, ...action.payload};
    },
    logout: state => {
      return {...initialState};
    },
  },
});

export const {login, logout} = userSlice.actions;

export default userSlice.reducer;
