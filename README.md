# Ionix Tasks

Aplicación desarrollada en React Native para gestión de tareas.

## Arquitectura / Patrones

- Desarrollado utilizando patrón Repository:
  - components: Componentes reutilizables dentro de la aplicación
  - queries: Llamada a servicios/recursos de API
  - features: Core de aplicación, lógicas de negocio
  - screens: páginas declarativas
  - store: manejo global de estado de aplicación

## Dependencias / librerías

- react-hook-form: para manejo de errores en formularios
- yup: para inyección de validaciones mediante esquemas
- react-query: manejo de peticiones a API, manejo de caché
- redux: para manejo de estado global de la aplicación
- json-server: Api mock

## Instalación

### Pre-requisitos

- NodeJs 16 o superior
- Yarn 1.22.X
- Watchman
- JDK 8
- Android Studio
- Android SDK Platform 32
- Android SDK Build-Tools 32.0.0
- Xcode 14
- CocoaPods

### Configuración inicial

Instalación de dependencias:

```
yarn install
```

Ingresar a carpeta ios

```
cd /ios
```

Instalar dependencias de iOS mediante CocoaPods

```
pos install
```

### Ejecutar Api

Para levantar la api se debe ejecutar:

```
yarn server
```

Levantará una api de manera local en el puerto 3005. Se debe crear archivo .env en raíz de proyecto a partir del archivo .env.example y modificar el campo API_URL

### Ejecutar App

Para ejecutar en android

```
yarn android
```

Para ejecutar en ios

```
yarn ios
```

Para ejecutar test

```
yarn test
```
