import React from 'react';

import {QueryClientProvider, QueryClient} from 'react-query';
import {Provider} from 'react-redux';
import {store} from './src/store/store';

import Navigator from './src/routes/routes';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const App = () => {
  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <Navigator />
      </QueryClientProvider>
    </Provider>
  );
};

export default App;
