import {canDeleteOrUpdate} from '../src/utils/canDeleteOrUpdate';

test('Admin try change task with status new', () => {
  // Given
  const isAdmin = true;
  const status = 'new';
  // When
  const result = canDeleteOrUpdate(isAdmin, status);
  // Then
  expect(result).toBe(true);
});

test('Operator try change task', () => {
  // Given
  const isAdmin = false;
  const status = 'new';
  // When
  const result = canDeleteOrUpdate(isAdmin, status);
  // Then
  expect(result).toBe(false);
});

test('Admin try change task with status assigned', () => {
  // Given
  const isAdmin = true;
  const status = 'assigned';
  // When
  const result = canDeleteOrUpdate(isAdmin, status);
  // Then
  expect(result).toBe(true);
});

test('Admin try change task with status waiting', () => {
  // Given
  const isAdmin = false;
  const status = 'waiting';
  // When
  const result = canDeleteOrUpdate(isAdmin, status);
  // Then
  expect(result).toBe(false);
});
