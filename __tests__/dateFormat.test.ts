import {dateFormat} from '../src/utils/dateFormat';

test('Get default date format', () => {
  // Given
  const date = '2023-03-08';
  // When
  const result = dateFormat(date);
  // Then
  expect(result).toBe('08/03/2023');
});

test('Get date format accorging format', () => {
  // Given
  const date = '2023-03-08';
  // When
  const result = dateFormat(date, 'DD-MM-YY');
  // Then
  expect(result).toBe('08-03-23');
});

test('Get invalid date format', () => {
  // Given
  const date = '';
  // When
  const result = dateFormat(date);
  // Then
  expect(result).toBe('Invalid date');
});
