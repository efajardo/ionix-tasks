import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import Action from '../src/components/Action';

test('render button and handle onPress', () => {
  // Given
  const data = {
    title: 'Test',
    onPress: jest.fn(),
  };

  // When
  const {getByText} = render(
    <Action text={data.title} onPress={data.onPress} />,
  );
  fireEvent(getByText('Test'), 'onPress');
  // Then
  expect(getByText('Test')).toBeTruthy();
  expect(data.onPress).toHaveBeenCalledTimes(1);
});
