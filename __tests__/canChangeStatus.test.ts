import {canChangeStatus} from '../src/utils/canChangeStatus';

test('Admin try change task status', () => {
  // Given
  const isAdmin = true;
  const status = 'new';
  // When
  const result = canChangeStatus(isAdmin, status);
  // Then
  expect(result).toBe(false);
});

test('Operator try change task with status new', () => {
  // Given
  const isAdmin = false;
  const status = 'new';
  // When
  const result = canChangeStatus(isAdmin, status);
  // Then
  expect(result).toBe(true);
});

test('Operator try change task with status assigned', () => {
  // Given
  const isAdmin = false;
  const status = 'assigned';
  // When
  const result = canChangeStatus(isAdmin, status);
  // Then
  expect(result).toBe(false);
});
