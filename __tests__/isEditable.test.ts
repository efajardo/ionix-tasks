import {isEditable} from '../src/utils/isEditable';

jest.mock('moment', () => {
  return () => jest.requireActual('moment')('2000-03-06T12:00:00.000Z');
});
// TODO: Revisar
// test('after', () => {
//   expect(isEditable('2024-05-07T12:00:00.000Z')).toBe(true);
// });
test('before', () => {
  expect(isEditable('dsvfx')).toBe(false);
});
