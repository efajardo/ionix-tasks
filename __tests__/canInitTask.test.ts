import {canInitTask} from '../src/utils/canInitTask';

test('Admin try initialize task', () => {
  // Given
  const isAdmin = true;
  const status = 'assigned';
  // When
  const result = canInitTask(isAdmin, status);
  // Then
  expect(result).toBe(false);
});

test('Operator try initialize task in status assigned', () => {
  // Given
  const isAdmin = false;
  const status = 'assigned';
  // When
  const result = canInitTask(isAdmin, status);
  // Then
  expect(result).toBe(true);
});

test('Operator try initialize task in status new', () => {
  // Given
  const isAdmin = false;
  const status = 'new';
  // When
  const result = canInitTask(isAdmin, status);
  // Then
  expect(result).toBe(false);
});

test('Operator try initialize task in status waiting', () => {
  // Given
  const isAdmin = false;
  const status = 'waiting';
  // When
  const result = canInitTask(isAdmin, status);
  // Then
  expect(result).toBe(false);
});
