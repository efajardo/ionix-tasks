import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import Button from '../src/components/Button';

test('render button', () => {
  // Given
  const data = {
    title: 'Test',
    onPress: jest.fn(),
  };

  // When
  const {getByText} = render(<Button title={data.title} onPress={jest.fn()} />);

  // Then
  expect(getByText('Test')).toBeTruthy();
});

test('call onPress function', () => {
  // Given
  const data = {
    title: 'Test',
    onPress: jest.fn(),
  };

  // When
  const {getByText} = render(
    <Button title={data.title} onPress={data.onPress} />,
  );
  fireEvent(getByText('Test'), 'onPress');

  // Then
  expect(data.onPress).toHaveBeenCalledTimes(1);
});
